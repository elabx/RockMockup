<?php namespace ProcessWire;
$info = array(
	'title' => 'Module for easy and fast mockup creation', 
	'summary' => 'Keyboard shortcuts and WYSIWYG editor',
	'version' => 1, 
	'author' => 'Bernhard Baumrock, baumrock.com', 
	'icon' => 'bolt',
	'autoload' => function() {
		$page = wire('page');
		if($page->template == 'admin' AND $page->id != 23) return true;
		else return false; 
	},
);
