<?php namespace ProcessWire;
/**
 * Module for easy and fast mockup creation
 * @author Bernhard Baumrock, baumrock.com
 */

class RockMockup extends WireData implements Module {

	/**
	 * Initialize the module
	 *
	 * ProcessWire calls this when the module is loaded. For 'autoload' modules, this will be called
	 * when ProcessWire's API is ready. As a result, this is a good place to attach hooks. 
	 *
	 */
	public function init() {
		// load ckeditor for editing
		$this->config->scripts->add($this->config->urls->InputfieldCKEditor . "ckeditor-" . InputfieldCKEditor::CKEDITOR_VERSION . '/ckeditor.js');
		$this->config->scripts->add('/site/modules/RockMockup/RockMockup.js');
		$this->config->styles->add('/site/modules/RockMockup/RockMockup.css');
	}
	
}
